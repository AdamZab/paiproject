<?php

require_once "Repository.php";
require_once __DIR__.'//..//Models//User.php';
require_once __DIR__.'//..//Models//Session.php';
require_once __DIR__.'//..//Models//SessionRow.php';

class SessionRepository extends Repository{

    public function getSessionRows($id_meeting): array {
        $result = [];
        $stmt = $this->database->connect()->prepare('
        Select mu.id_meeting_to_user, m.id_meeting, m.name, u.id_user, u.name, u.surname, mu.money_amount, mu.id_meeting_to_user
	        FROM meeting m 
	        left JOIN meeting_to_user mu on m.id_meeting=mu.fk_id_meeting 
            left JOIN user u on mu.fk_id_user=u.id_user
	        WHERE id_meeting =:id_meeting
        ');

        $stmt->bindParam(':id_meeting', $id_meeting, PDO::PARAM_STR);
        $stmt->execute();

        $sessionRows = $stmt->fetchAll(PDO::FETCH_ASSOC);

        foreach ($sessionRows as $row) {
            $result[] = new SessionRow(
                $row['name'],
                $row['id_user'],
                $row['surname'],
                number_format($row['money_amount'], 2),
                $row['id_meeting_to_user']
            );
        }
        return $result;
    }

    public function leaveSession($id_meeting){
        $userEmail = $_SESSION['id'];
        $getStmt = $this->database->connect()->prepare('
            SELECT id_user FROM `user` WHERE email = :userEmail
        ');

        $getStmt->bindParam(':userEmail', $userEmail, PDO::PARAM_STR);
        $getStmt->execute();

        $id_userArray= $getStmt->fetch(PDO::FETCH_ASSOC);
        $id=$id_userArray['id_user'];

        $stmt = $this->database->connect()->prepare('
            DELETE FROM `meeting_to_user` WHERE fk_id_user = :id and fk_id_meeting = :id_meeting
        ');

        $stmt->bindParam(':id_meeting', $id_meeting, PDO::PARAM_INT);
        $stmt->bindParam(':id', $id, PDO::PARAM_INT);
        $stmt->execute();
    }

    public function deleteSession($id_meeting){
        $stmt = $this->database->connect()->prepare('
            DELETE FROM meeting WHERE id_meeting = :id_meeting
        ');

        $stmt->bindParam(':id_meeting', $id_meeting, PDO::PARAM_INT);
        $stmt->execute();
    }

    public function getUserRoleInMeeting($id_meeting){
        $userEmail = $_SESSION['id'];
        $getStmt = $this->database->connect()->prepare('
            SELECT id_user FROM `user` WHERE email = :userEmail
        ');

        $getStmt->bindParam(':userEmail', $userEmail, PDO::PARAM_STR);
        $getStmt->execute();

        $id_userArray= $getStmt->fetch(PDO::FETCH_ASSOC);
        $id=$id_userArray['id_user'];

        $stmt = $this->database->connect()->prepare('
            SELECT user_role FROM meeting_to_user 
                WHERE fk_id_meeting = :id_meeting and fk_id_user = :id
        ');

        $stmt->bindParam(':id_meeting', $id_meeting, PDO::PARAM_INT);
        $stmt->bindParam(':id', $id, PDO::PARAM_INT);
        $stmt->execute();
        $id_meetingArray= $stmt->fetch(PDO::FETCH_ASSOC);
        return $id_meetingArray['user_role'];
    }

    public function addUserToSession($id_user, $id_meeting){
        
        $stmt = $this->database->connect()->prepare('
        INSERT INTO `meeting_to_user` 
	        (`money_amount`, `fk_id_user`, `fk_id_meeting`, `user_role`) 
	        VALUES ("0.00", :id_user, :id_meeting, "2");
        ');

        $stmt->bindParam(':id_meeting', $id_meeting, PDO::PARAM_INT);
        $stmt->bindParam(':id_user', $id_user, PDO::PARAM_INT);
        $stmt->execute();
    }


    public function getSessionSummary($id_meeting){
        $sessionRows = [];
        $sessionRows = $this->getSessionRows($id_meeting);
        
        $sumMoney = 0;
        foreach ($sessionRows as $sessionRow) {
            $sumMoney = $sumMoney + (double)$sessionRow->getAmount();
        }
        $sumMoney = $sumMoney/count($sessionRows);

        $sumarryRows = [];
        foreach ($sessionRows as $row) {
            $sumarryRows[] = new SessionRow(
                $row->getUserName(),
                $row->getUserId(),
                $row->getUserSurname(),
                ($sumMoney-(double)$row->getAmount()),
                $row->getSessionRowId()
            );
        }
        return $sumarryRows;
    }

    public function increaseMoneyAmount($id_meeting, $money_amount)
    {
        $userEmail = $_SESSION['id'];
        $getStmt = $this->database->connect()->prepare('
            SELECT id_user FROM `user` WHERE email = :userEmail
        ');

        $getStmt->bindParam(':userEmail', $userEmail, PDO::PARAM_STR);
        $getStmt->execute();

        $id_userArray= $getStmt->fetch(PDO::FETCH_ASSOC);
        $id=$id_userArray['id_user'];

        $stmt = $this->database->connect()->prepare('
            UPDATE meeting_to_user
                SET money_amount = money_amount + :money_amount 
                WHERE fk_id_meeting = :id_meeting and fk_id_user = :id;
        ');

        $stmt->bindParam(':id', $id, PDO::PARAM_INT);
        $stmt->bindParam(':id_meeting', $id_meeting, PDO::PARAM_INT);
        $stmt->bindParam(':money_amount', $money_amount, PDO::PARAM_STR);
        $stmt->execute();
    }

    public function decreaseMoneyAmount($id_meeting, $money_amount)
    {
        $userEmail = $_SESSION['id'];
        $getStmt = $this->database->connect()->prepare('
            SELECT id_user FROM `user` WHERE email = :userEmail
        ');

        $getStmt->bindParam(':userEmail', $userEmail, PDO::PARAM_STR);
        $getStmt->execute();

        $id_userArray= $getStmt->fetch(PDO::FETCH_ASSOC);
        $id=$id_userArray['id_user'];

        $stmt = $this->database->connect()->prepare('
            UPDATE meeting_to_user 
                SET money_amount = money_amount - :money_amount 
                WHERE fk_id_meeting = :id_meeting and fk_id_user = :id;
        ');

        $stmt->bindParam(':id', $id, PDO::PARAM_INT);
        $stmt->bindParam(':id_meeting', $id_meeting, PDO::PARAM_INT);
        $stmt->bindParam(':money_amount', $money_amount, PDO::PARAM_STR);
        $stmt->execute();
    }
}