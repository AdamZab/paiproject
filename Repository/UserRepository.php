<?php

require_once "Repository.php";
require_once __DIR__.'//..//Models//User.php';

class UserRepository extends Repository {

    public function getUser(string $email): ?User 
    {
        $stmt = $this->database->connect()->prepare('
            SELECT * FROM user u 
                LEFT JOIN user_salt us on u.id_user =  us.fk_id_user
                WHERE email = :email
        ');
        $stmt->bindParam(':email', $email, PDO::PARAM_STR);
        $stmt->execute();

        $user = $stmt->fetch(PDO::FETCH_ASSOC);

        if($user == false) {
            return null;
        }

        return new User(
            $user['email'],
            $user['password'],
            $user['name'],
            $user['surname'],
            $user['role'],
            $user['id_user'],
            $user['salt']
        );
    }

    public function createUser(string $name, string $surname, string $email, string $password, string $salt)
    {
        $stmt = $this->database->connect()->prepare('
            INSERT INTO user (name, surname, email, role, password, create_date)
                    VALUES (:name, :surname, :email, "1", :password, NOW());
            INSERT INTO user_salt (fk_id_user, salt)
                    VALUES (LAST_INSERT_ID(), :salt);
        ');

        $stmt->bindParam(':email', $email, PDO::PARAM_STR);
        $stmt->bindParam(':name', $name, PDO::PARAM_STR);
        $stmt->bindParam(':surname', $surname, PDO::PARAM_STR);
        $stmt->bindParam(':password', $password, PDO::PARAM_STR);
        $stmt->bindParam(':salt', $salt, PDO::PARAM_STR);

        $stmt->execute();
    }

    public function getUsers(): array {
        $userEmail = $_SESSION['id'];
        $getStmt = $this->database->connect()->prepare('
            SELECT id_user FROM `user` WHERE email = :userEmail
        ');

        $getStmt->bindParam(':userEmail', $userEmail, PDO::PARAM_STR);
        $getStmt->execute();

        $id_userArray= $getStmt->fetch(PDO::FETCH_ASSOC);
        $id=$id_userArray['id_user'];

        $result = [];
        $stmt = $this->database->connect()->prepare('
            SELECT * FROM user WHERE id_user != :id
        ');
        $stmt->bindParam(':id', $id, PDO::PARAM_INT);
        $stmt->execute();
        $users = $stmt->fetchAll(PDO::FETCH_ASSOC);

        foreach ($users as $user) {
            $result[] = new User(
                $user['email'],
                $user['password'],
                $user['name'],
                $user['surname'],
                $user['role'],
                $user['id_user']
            );
        }

        return $result;
    }

    public function getNotFriends(): array {

        $userEmail = $_SESSION['id'];
        $getStmt = $this->database->connect()->prepare('
            SELECT id_user FROM `user` WHERE email = :userEmail
        ');

        $getStmt->bindParam(':userEmail', $userEmail, PDO::PARAM_STR);
        $getStmt->execute();

        $id_userArray= $getStmt->fetch(PDO::FETCH_ASSOC);
        $id=$id_userArray['id_user'];

        $result = [];
        $stmt = $this->database->connect()->prepare('
            Select * FROM user 
            WHERE id_user not in ( 
                SELECT id_user_to FROM `friends` WHERE id_user_from=:id 
            ) 
            AND id_user != :id
        ');
        $stmt->bindParam(':id', $id, PDO::PARAM_INT);

        $stmt->execute();
        $users = $stmt->fetchAll(PDO::FETCH_ASSOC);

        foreach ($users as $user) {
            $result[] = new User(
                $user['email'],
                $user['password'],
                $user['name'],
                $user['surname'],
                $user['role'],
                $user['id_user']
            );
        }

        return $result;
    }

    public function getFriends(): array {
        $userEmail = $_SESSION['id'];
        $getStmt = $this->database->connect()->prepare('
            SELECT id_user FROM `user` WHERE email = :userEmail
        ');

        $getStmt->bindParam(':userEmail', $userEmail, PDO::PARAM_STR);
        $getStmt->execute();

        $id_userArray= $getStmt->fetch(PDO::FETCH_ASSOC);
        $id=$id_userArray['id_user'];

        $result = [];
        $stmt = $this->database->connect()->prepare('
            Select * FROM user 
            WHERE id_user in (
                SELECT id_user_to FROM `friends` 
                WHERE id_user_from=:id
            ) 
        ');
        $stmt->bindParam(':id', $id, PDO::PARAM_INT);

        $stmt->execute();
        $users = $stmt->fetchAll(PDO::FETCH_ASSOC);

        foreach ($users as $user) {
            $result[] = new User(
                $user['email'],
                $user['password'],
                $user['name'],
                $user['surname'],
                $user['role'],
                $user['id_user']
            );
        }

        return $result;
    }

    public function addFriend($friend_Id)
    {
        $userEmail = $_SESSION['id'];
        $getStmt = $this->database->connect()->prepare('
            SELECT id_user FROM `user` WHERE email = :userEmail
        ');

        $getStmt->bindParam(':userEmail', $userEmail, PDO::PARAM_STR);
        $getStmt->execute();

        $id_userArray= $getStmt->fetch(PDO::FETCH_ASSOC);
        $id=$id_userArray['id_user'];

        $stmt = $this->database->connect()->prepare('
            INSERT INTO `friends` (`id_user_from`, `id_user_to`) 
                VALUES (:id, :friend_Id)
        ');

        $stmt->bindParam(':id', $id, PDO::PARAM_INT);
        $stmt->bindParam(':friend_Id', $friend_Id, PDO::PARAM_INT);

        $stmt->execute();
    }

    public function deleteUser($user_id)
    {
        $stmt = $this->database->connect()->prepare('
            DELETE FROM `user` WHERE `user`.`id_user` = :user_id
        ');

        $stmt->bindParam(':user_id', $user_id, PDO::PARAM_INT);

        $stmt->execute();
    }
}