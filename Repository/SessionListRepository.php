<?php

require_once "Repository.php";
require_once __DIR__.'//..//Models//User.php';
require_once __DIR__.'//..//Models//Session.php';
require_once __DIR__.'//..//Models//SessionRow.php';

class SessionListRepository extends Repository{

    public function getSessions(): array {
        $userEmail = $_SESSION['id'];
        $getStmt = $this->database->connect()->prepare('
            SELECT id_user FROM `user` WHERE email = :userEmail
        ');

        $getStmt->bindParam(':userEmail', $userEmail, PDO::PARAM_STR);
        $getStmt->execute();

        $id_userArray= $getStmt->fetch(PDO::FETCH_ASSOC);
        $id=$id_userArray['id_user'];
        
        $result = [];
        $stmt = $this->database->connect()->prepare('
        SELECT * FROM meeting 
        WHERE id_meeting in (
            SELECT fk_id_meeting FROM `meeting_to_user` 
            WHERE fk_id_user=:id
        ) 
        ');

        $stmt->bindParam(':id', $id, PDO::PARAM_INT);
        $stmt->execute();

        $sessions = $stmt->fetchAll(PDO::FETCH_ASSOC);

        foreach ($sessions as $session) {
            $result[] = new Session(
                $session['name'],
                $session['id_meeting']
            );
        }

        return $result;
    }

    public function createSession(string $name)
    {
        $userEmail = $_SESSION['id'];
        $getStmt = $this->database->connect()->prepare('
            SELECT id_user FROM `user` WHERE email = :userEmail
        ');

        $getStmt->bindParam(':userEmail', $userEmail, PDO::PARAM_STR);
        $getStmt->execute();

        $id_userArray= $getStmt->fetch(PDO::FETCH_ASSOC);
        $id_user=$id_userArray['id_user'];


        
        $stmt = $this->database->connect()->prepare('
            INSERT INTO meeting (name)
                VALUES (:name);
            INSERT INTO meeting_to_user (money_amount, fk_id_user, fk_id_meeting, user_role)
            VALUES (0, :id_user, LAST_INSERT_ID(), 1);
        ');

        $stmt->bindParam(':name', $name, PDO::PARAM_STR);
        $stmt->bindParam(':id_user', $id_user, PDO::PARAM_INT);

        $stmt->execute();
    }
}