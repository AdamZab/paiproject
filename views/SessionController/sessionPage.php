<?php
    if(!isset($_SESSION['id']) and !isset($_SESSION['role'])) {
        die('You are not logged in!');
    }
?>

<!DOCTYPE html>
<head>
    <meta charset="UTF-8">
    <link rel="Stylesheet" type="text/css" href="Public/css/sessionPage.css" />
    <link rel="Stylesheet" type="text/css" href="Public/css/menu.css" />
    <script type="text/javascript" src="Public/js/sessionPage.js"></script>
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <title>befair</title>
</head>
<div class="container">
    <div id = "sidebar">
        <ul>
            <li><div class="tab">
                <img src="Public/img/username tab.svg">
            </div></li>
            <li><div class="tab">
                <a href="?page=board" target="_self">
                    <img src="Public/img/sessions button.svg">
                </a>
            </div></li>
            <li><div class="tab">
                <a href="?page=friends" target="_self">
                    <img src="Public/img/friends button.svg">
                </a>
            </div></li>
                <li><div class="tab">
                <a href="?page=logout" target="_self">
                    <img src="Public/img/logout button.svg">
                </a>
            </div></li>
        </ul>
    </div>
    <div class="view-container">
        <div class="container-holder">
            <table>
                <tr>
                    <th>Person</th>
                    <th>Amount</th>
                </tr>
                <?php foreach($sessionRows as $row) {?>
                <tr>
                    <td><?php echo $row->getUserName() . " " . $row->getUserSurname();?></td>
                    <td><?php echo $row->getAmount();?></td>
                </tr>
                <?php  } ?>
            </table>

            <div class="sesssion-control-container">
                <a href="?page=addUser&sessionId=<?php echo $sessionId ?>" target="_self">
                    <img src="Public/img/friends/Add Friend btn.svg" width="150" height="30">
                </a>
                <form method="POST" action="?page=session&sessionId=<?php echo $sessionId; ?>">
                    <label>
                        <input type="number" placeholder="0.00" required name="price" min="0" value="0" step="0.01" title="Currency" pattern="^\d+(?:\.\d{1,2})?$" 
                        onblur="this.parentNode.parentNode.style.backgroundColor=/^\d+(?:\.\d{1,2})?$/.test(this.value)?'inherit':'red'">
                    </label>
                    <input class="add-button" type="submit" value="" name="add">
                    <input class="remove-button" type="submit" value = "" name="remove">
                </form>
                <a href="?page=summarize&sessionId=<?php echo $sessionId ?>" target="_self">
                    <img src="Public/img/session/Summarize btn.svg" width="150" height="30">
                </a>
                <a href="?page=session&deleteId=<?php echo $sessionId ?>" target="_self">
                    <img src="Public/img/session/Delete Session btn.svg" width="150" height="30">
                </a>
            </div>
        </div>    
    </div>
                </div>