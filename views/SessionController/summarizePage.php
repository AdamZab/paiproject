<?php
    if(!isset($_SESSION['id']) and !isset($_SESSION['role'])) {
        die('You are not logged in!');
    }
?>

<!DOCTYPE html>
<head>
    <meta charset="UTF-8">
    <link rel="Stylesheet" type="text/css" href="Public/css/sessionPage.css" />
    <link rel="Stylesheet" type="text/css" href="Public/css/menu.css" />
    <script type="text/javascript" src="Public/js/sessionPage.js"></script>
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <title>befair</title>
</head>
<div class="container">
    <div id = "sidebar">
        <ul>
            <li><div class="tab">
                <img src="Public/img/username tab.svg">
            </div></li>
            <li><div class="tab">
                <a href="?page=board" target="_self">
                    <img src="Public/img/sessions button.svg">
                </a>
            </div></li>
            <li><div class="tab">
                <a href="?page=friends" target="_self">
                    <img src="Public/img/friends button.svg">
                </a>
            </div></li>
                <li><div class="tab">
                <a href="?page=logout" target="_self">
                    <img src="Public/img/logout button.svg">
                </a>
            </div></li>
        </ul>
    </div>

    <table>
        <tr>
            <th>Name</th>
            <th>Balance (-X equals amount that others have to pay you)</th>
        </tr>
        <?php
            foreach($summarizeRows as $row) {?>
            <tr>
                <td><?php echo $row->getUserName() . " " . $row->getUserSurname();?></td>
                <td><?php echo number_format($row->getAmount(), 2, ',', '');?></td>
            </tr>
        <?php  } ?>
    </table>

    <div>
        <a href="?page=session&deleteId=<?php echo $sessionId ?>" target="_self">
            <img src="Public/img/session/Delete Session btn.svg" width="150" height="30">
        </a>

    </div>

</div>