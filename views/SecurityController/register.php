<!DOCTYPE html>
<head>
    <meta charset="UTF-8">
    <link rel="Stylesheet" type="text/css" href="Public/css/style.css" />
    <script type="text/javascript" src="Public/js/navigation.js"></script>
    <script type="text/javascript" src="Public/js/validate.js" defer></script>
    <title>befair</title>
</head>
<body>
<div class="container">
    <div class="logo">
        <img src="Public/img/logo.svg">
    </div>
    <form method="POST" action="?page=register">
        <div class="messages">
            <?php
                if(isset($messages)){
                    foreach($messages as $message) {
                        echo $message;
                    }
                }
            ?>
        </div>
        <input name="name" type="text" placeholder="Name">
        <input name="surname" type="text" placeholder="Surname">
        <input name="email" type="text" placeholder="email@email.com">
        <input name="password" type="password" placeholder="Password">
        <input name="repeat-password" type="password" placeholder="Repeat Password">
        <button type="submit">CONTINUE</button>
        <div class="signin">
            <p>Already have account? <a href="?page=login">Sign in</a>.</p>
        </div>
    </form>
</div>
</body>
</html>