<!DOCTYPE html>
<head>
    <meta charset="UTF-8">
    <link rel="Stylesheet" type="text/css" href="Public/css/style.css" />
    <script type="text/javascript" src="Public/js/navigation.js"></script>
    <script type="text/javascript" src="Public/js/validate.js" defer></script>
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <title>befair</title>
</head>
<body>
<div class="container">
    <div class="logo">
        <img src="Public/img/logo.svg">
    </div>
    <form action="?page=login" method="POST">
        <div class="messages">
            <?php
                if(isset($messages)){
                    foreach($messages as $message) {
                        echo $message;
                    }
                }
            ?>
        </div>
        <input name="email" type="text" placeholder="email@email.com">
        <input name="password" type="password" placeholder="Password">
        <button type="submit">CONTINUE</button>
        <div class="signin">
            <p>Don't have account? <a href="?page=register">Register</a>.</p>
        </div>
    </form>
</div>
</body>
</html>