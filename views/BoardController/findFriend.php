<?php
    if(!isset($_SESSION['id']) and !isset($_SESSION['role'])) {
        die('You are not logged in!');
    }    
?>

<!DOCTYPE html>
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="Stylesheet" type="text/css" href="Public/css/menu.css" />
    <link rel="Stylesheet" type="text/css" href="Public/css/searchFriend.css" />
    <script type="text/javascript" src="Public/js/searchTable.js"></script>
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

</head>
<body>
<div class="container">
  <div id = "sidebar">
    <ul>
        <li><div class="tab">
            <img src="Public/img/username tab.svg">
        </div></li>
        <li><div class="tab">
            <a href="?page=board" target="_self">
                <img src="Public/img/sessions button.svg">
            </a>
        </div></li>
        <li><div class="tab">
            <a href="?page=friends" target="_self">
                <img src="Public/img/friends button.svg">
            </a>
        </div></li>
        <li><div class="tab">
            <a href="?page=logout" target="_self">
                <img src="Public/img/logout button.svg">
            </a>
        </div></li>
    </ul>
  </div>
  <input type="text" id="myInput" onkeyup="search()" placeholder="Search for names.." title="Type in a name">

  <table id="myTable"style="overflow-y:auto;">
    <tr class="header">
      <th style="width:45%;">Name</th>
      <th style="width:45%;">Email</th>
      <th style="width=10%;">Add Friend</th>
    </tr>
    <?php
     foreach($userFriend as $row) {?>
      <tr>
      <td><?php echo $row->getName() . " " . $row->getSurname();?></td>
      <td><?php echo $row->getEmail();?></td>
      <td><a href="?page=friends&id=<?php echo $row->getId(); ?>"> <input type="button" value="Add Friend"/></a></td>
      </tr>
      <?php  } ?>
  </table>
  
</div>
</body>
</html>