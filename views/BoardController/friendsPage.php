<?php
    if(!isset($_SESSION['id']) and !isset($_SESSION['role'])) {
        die('You are not logged in!');
    }
?>

<!DOCTYPE html>
<head>
    <meta charset="UTF-8">
    <link rel="Stylesheet" type="text/css" href="Public/css/style.css" />
    <link rel="Stylesheet" type="text/css" href="Public/css/menu.css" />
    <link rel="Stylesheet" type="text/css" href="Public/css/friendsList.css" />
    <script type="text/javascript" src="../../Public/js/friendsList.js"></script>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <title>befair</title>
</head>
<div class="container">
<div id = "sidebar">
    <ul>
        <li><div class="tab">
            <img src="Public/img/username tab.svg">
        </div></li>
        <li><div class="tab">
            <a href="?page=board" target="_self">
                <img src="Public/img/sessions button.svg">
            </a>
        </div></li>
        <li><div class="tab">
            <a href="?page=friends" target="_self">
                <img src="Public/img/friends button.svg">
            </a>
        </div></li>
        <li><div class="tab">
            <a href="?page=logout" target="_self">
                <img src="Public/img/logout button.svg">
            </a>
        </div></li>
    </ul>
</div>

<div class="w3-container">
  <nav>
  <ul class="w3-ul w3-hoverable">
  <?php
     foreach($userFriend as $row) {?>
    <li><?php echo $row->getName() . " " . $row->getSurname();?></li>
    <?php  } ?>
  </ul>
</nav>
<a href="?page=addFriend" target="_self">
    <img src="Public/img/friends/Add Friend btn.svg">
</a>
</div>

</div>