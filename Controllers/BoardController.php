<?php

require_once 'AppController.php';
require_once __DIR__.'//..//Database.php';
require_once __DIR__.'//..//Models//User.php';
require_once __DIR__.'//..//Repository//UserRepository.php';

class BoardController extends AppController {

    public function showSessionList()
    {
        $this->render('sessionsPage');
    }
    
    public function users()
    {
        if ($this->isGet()) {
            $userRepository = new UserRepository(); 
            if(isset($_GET['id'])) {
                $id = $_GET['id'];
                $userRepository->deleteUser($id);
                header("Location: ?page=users");
            } 
            $users = $userRepository->getUsers();
            $this->render('users', ['users' => $users]);
        }
        $this->render('users');
    }

    public function createSession()
    {
        $this->render('createSession');
    }

    public function showAddUserTable(){
        if ($this->isGet()) {
            $userRepository = new UserRepository(); 
            if(isset($_GET['sessionId'])){
                $sessionId = $_GET['sessionId'];
                $userFriend = $userRepository->getFriends();
                $this->render('addUser', ['userFriend' => $userFriend, 'sessionId' => $sessionId]);
            }
        }
        $this->render('addUser');
    }

    public function showFriendsList()
    {
        if ($this->isGet()) {
            $userRepository = new UserRepository(); 
            if(isset($_GET['id'])) {
                $id = $_GET['id'];
                $userRepository->addFriend($id);
                header("Location: ?page=friends");
            } 
            $userFriend = $userRepository->getFriends();
            $this->render('friendsPage', ['userFriend' => $userFriend]);
        }
        $this->render('friendsPage');
    }

    public function showAddFriendTable(){
        if ($this->isGet()) {
            $userRepository = new UserRepository(); 
            $userFriend = $userRepository->getNotFriends();
            $this->render('findFriend', ['userFriend' => $userFriend]);
        }
        $this->render('findFriend');
    }

    public function addFriend(int $friend_Id)
    {
        $userRepository = new UserRepository();
        $this->userRepository->addFriend($friend_Id);
        http_response_code(200);
    }
}