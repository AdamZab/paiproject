<?php

require_once 'AppController.php';
require_once __DIR__.'//..//Models//User.php';
require_once __DIR__.'//..//Repository//UserRepository.php';

class SecurityController extends AppController {

    public function login()
    {
        $userRepository = new UserRepository();

        if ($this->isPost()) {
            $email = $_POST['email'];
            
            $user = $userRepository->getUser($email);

            if (!$user) {
                $this->render('login', ['messages' => ['User with this email not exist!']]);
                return;
            }

            $password = md5($user->getSalt() . $_POST['password']);

            if ($user->getPassword() !== $password) {
                $this->render('login', ['messages' => ['Wrong password!']]);
                return;
            }
            $_SESSION["id"] = $user->getEmail();
            $_SESSION["role"] = $user->getRole();

            header("Location: ?page=board");
            
            return;
        }

        $this->render('login');
    }
    public function register()
    {
        if ($this->isPost()) {
            $permitted_chars = '0123456789abcdefghijklmnopqrstuvwxyz';
            $salt = substr(str_shuffle($permitted_chars), 0, 10);

            $userRepository = new UserRepository();
            $name = "";
            $surname = "";
            $email = "";
            $password = "";
            $repeat_password = "";
            $name = $_POST['name'];
            $surname = $_POST['surname'];
            $email = $_POST['email'];
            $password = md5($salt . $_POST['password']);
            $repeat_password = md5($salt . $_POST['repeat-password']);


            if(empty($name)){
                $this->render('register', ['messages' => ['You have to fill the name!']]);
                return;
            }

            if(empty($surname)){
                $this->render('register', ['messages' => ['You have to fill the surname!']]);
                return;
            }

            if(empty($email)){
                $this->render('register', ['messages' => ['You have to fill the email!']]);
                return;
            }

            if(empty($password)){
                $this->render('register', ['messages' => ['You have to fill the password!']]);
                return;
            }

            if(empty($repeat_password)){
                $this->render('register', ['messages' => ['You have to fill the repeated password!']]);
                return;
            }

            if($password != $repeat_password){
                $this->render('register', ['messages' => ['Password have to be the same!']]);
                return;
            }

            $user = $userRepository->getUser($email);
            if ($user) {
                $this->render('register', ['messages' => ['User with this email already exist!']]);
                return;
            }

            $userRepository->createUser($name, $surname, $email, $password, $salt);
            header("Location: ?page=login");
            
            return;
        }

        $this->render('register');
    }

    public function logout(){
        session_unset();
        session_destroy();

        $this->render('login', ['messages' => ['You have been successfully logged out.']]);
    }
}