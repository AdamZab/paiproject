<?php

require_once 'AppController.php';
require_once __DIR__.'//..//Database.php';
require_once __DIR__.'//..//Models//User.php';
require_once __DIR__.'//..//Models//Session.php';
require_once __DIR__.'//..//Models//SessionRow.php';
require_once __DIR__.'//..//Repository//SessionRepository.php';
require_once __DIR__.'//..//Repository//SessionListRepository.php';

class SessionController extends AppController {

    public function showSummarizePage()
    {
        if ($this->isGet()) {
            if(isset($_GET['sessionId'])) {
                $sessionId = $_GET['sessionId'];
                $sessionRepository = new SessionRepository();
                $summarizeRows = $sessionRepository->getSessionSummary($sessionId);
                $this->render('summarizePage', ['summarizeRows' => $summarizeRows, 'sessionId' => $sessionId]);
            }
        }
        $this->render('summarizePage');
    }

    public function showSessionPage()
    {
        if($this->isPost()){
            if(isset($_POST['price']) && isset($_GET['sessionId'])){
                $sessionRepository = new SessionRepository();
                $sessionId = $_GET['sessionId'];
                $price = $_POST['price'];
                
                if(isset($_POST['add'])){
                    $sessionRepository->increaseMoneyAmount($sessionId, $price);
                }elseif(isset($_POST['remove'])){
                    $sessionRepository->decreaseMoneyAmount($sessionId, $price);
                }
                
                $sessionRows = $sessionRepository->getSessionRows($sessionId);
                $this->render('sessionPage', ['sessionRows' => $sessionRows, 'sessionId' => $sessionId]);
            }
        }
        if ($this->isGet()) {
            $sessionRepository = new SessionRepository();
            if(isset($_GET['sessionId'])) {
                $sessionId = $_GET['sessionId'];
                if(isset($_GET['id'])){
                    $id = $_GET['id'];
                    $sessionRepository->addUserToSession($id, $sessionId);
                    header("Location: ?page=session&sessionId=".$sessionId);
                }
                $sessionRows = $sessionRepository->getSessionRows($sessionId);
                $this->render('sessionPage', ['sessionRows' => $sessionRows, 'sessionId' => $sessionId]);
            }elseif(isset($_GET['deleteId'])) {
                $deleteId = $_GET['deleteId'];

                $role = $sessionRepository->getUserRoleInMeeting($deleteId);
                if($role==1){
                    $sessionRepository->deleteSession($deleteId);
                }else{
                    $sessionRepository->leaveSession($deleteId);
                }
                header("Location: ?page=board");
            }
        }
        $this->render('sessionPage');
    }

    public function showSessionList()
    {
        if ($this->isGet()) {
            $sessionListRepository = new SessionListRepository(); 
            $sessions = $sessionListRepository->getSessions();
            $this->render('sessionsPage', ['sessions' => $sessions]);
        }
        $this->render('sessionsPage');
    }

    public function createSession()
    {
        if ($this->isPost()) {
            $sessionListRepository = new SessionListRepository();
            $name = "";
            $name = $_POST['name'];

            if(empty($name)){
                $this->render('createSession', ['messages' => ['You have to fill the name!']]);
                return;
            }
            
            $sessionListRepository->createSession($name);
            header("Location: ?page=board");
            
            return;
        }
        $this->render('createSession');
    }
}
