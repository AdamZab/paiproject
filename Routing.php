<?php

require_once 'Controllers//BoardController.php';
require_once 'Controllers//SecurityController.php';
require_once 'Controllers//SessionController.php';

class Routing {
    private $routes = [];

    public function __construct()
    {
        $this->routes = [
            'board' => [
                'controller' => 'SessionController',
                'action' => 'showSessionList'
            ],
            'friends' => [
                'controller' => 'BoardController',
                'action' => 'showFriendsList'
            ],
            'addFriend' => [
                'controller' => 'BoardController',
                'action' => 'showAddFriendTable'
            ],
            'session' => [
                'controller' => 'SessionController',
                'action' => 'showSessionPage'
            ],
            'createSession' => [
                'controller' => 'SessionController',
                'action' => 'createSession'
            ],
            'addUser' => [
                'controller' => 'BoardController',
                'action' => 'showAddUserTable'
            ],
            'summarize' => [
                'controller' => 'SessionController',
                'action' => 'showSummarizePage'
            ],
            'login' => [
                'controller' => 'SecurityController',
                'action' => 'login'
            ],
            'register' => [
                'controller' => 'SecurityController',
                'action' => 'register'
            ],
            'logout' => [
                'controller' => 'SecurityController',
                'action' => 'logout'
            ],
            'users' => [
                'controller' => 'BoardController',
                'action' => 'users'
            ]
        ];
    }

    public function run()
    {
        $page = isset($_GET['page']) ? $_GET['page'] : 'index';

        if (isset($this->routes[$page])) {
            $controller = $this->routes[$page]['controller'];
            $action = $this->routes[$page]['action'];

            $object = new $controller;
            $object->$action();
        }
    }
}