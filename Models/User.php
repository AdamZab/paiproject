<?php

class User {
    private $id_user;
    private $email;
    private $password;
    private $name;
    private $surname;
    private $role;
    private $salt;

    public function __construct(
        string $email,
        string $password,
        string $name,
        string $surname,
        string $role,
        int $id_user = null,
        string $salt = null
    ) {
        $this->email = $email;
        $this->password = $password;
        $this->name = $name;
        $this->surname = $surname;
        $this->role = $role;
        $this->id_user = $id_user;
        $this->salt = $salt;
    }

    public function getEmail(): string 
    {
        return $this->email;
    }

    public function getPassword()
    {
        return $this->password;
    }

    public function getRole()
    {
        return $this->role;
    }

    public function getName()
    {
        return $this->name;
    }

    public function getSurname()
    {
        return $this->surname;
    }

    public function getId()
    {
        return $this->id_user;
    }

    public function getSalt()
    {
        return $this->salt;
    }
}