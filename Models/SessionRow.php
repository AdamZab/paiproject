<?php

class SessionRow{
    private $id;
    private $userName;
    private $userSurname;
    private $userId;
    private $amount;

    public function __construct(
        string $userName,
        int $userId,
        string $userSurname,
        string  $amount,
        int $id = null
    ) {
        $this->userName = $userName;
        $this->userId = $userId;
        $this->userSurname = $userSurname;
        $this->amount = $amount;
        $this->id = $id;
    }

    public function getUserName(): string 
    {
        return $this->userName;
    }

    public function getUserSurname(): string
    {
        return $this->userSurname;
    }

    public function getUserId(): int
    {
        return $this->userId;
    }
    
    public function getAmount(): string 
    {
        return $this->amount;
    }

    public function getSessionRowId(): int{
        return $this->id;
    }
}