<?php

class Session{
    private $id;
    private $name;

    public function __construct(
        string $name,
        int $id = null
    ){
        $this->name=$name;
        $this->id = $id;
    }
    
    public function getName(): string 
    {
        return $this->name;
    }
    
    public function getId() 
    {
        return $this->id;
    }

}