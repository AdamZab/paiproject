<?php

class SummaryRow{
    private $userName;
    private $userSurname;
    private $userId;
    private $amount;
    
    
    public function __construct(
        string $userName,
        int $userId,
        string $userSurname,
        string $amount
    ) {
        $this->userName = $userName;
        $this->userId = $userId;
        $this->userSurname = $userSurname;
        $this->amount = $amount;
    }

    public function getUserName(): string 
    {
        return $this->userName;
    }

    public function getUserSurname(): string
    {
        return $this->userSurname;
    }

    public function getUserId(): int
    {
        return $this->userId;
    }
    
    public function getAmount(): string
    {
        return $this->amount;
    }
}